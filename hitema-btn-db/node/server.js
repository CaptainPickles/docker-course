
var cors = require('cors')
const { Sequelize, DataTypes } = require('sequelize');
const app = require('express')();
const http = require('http').Server(app);
const io = require('socket.io')(http,{  cors: {
    origin: true,
    methods: ["GET", "POST"]
  }});
const port = 3000
let click = 0

app.use(cors())
const sequelize = new Sequelize('postgres://root:root@postgres:5432/root') 
connectToDb()

const clickModel = sequelize.define('Click', {
    id: {
        type: DataTypes.STRING,
        primaryKey:true,
        allowNull: false
    },
    // Model attributes are defined here
    click: {
        type: DataTypes.BIGINT,
        allowNull: false
    },
}, {

    // Other model options go here
});
seed()
let users = []

io.on('connection', async (socket) => {
    console.log('a user connected ip ',socket.request.connection.remoteAddress);
    let userClick = await clickModel.findByPk(socket.request.connection.remoteAddress )
    if(userClick === null) userClick = await clickModel.create({ id: socket.request.connection.remoteAddress, click: 0 })
    users.push({ip:userClick.id,click: userClick.click})
    socket.emit('scores',users);
    io.to(socket.id).emit("score",userClick.click)

    socket.on('click', async (msg) => {
        userClick.click++
        io.to(socket.id).emit("score",userClick.click)
        users = users.map(user => {
    
            if(user &&  user.ip === socket.request.connection.remoteAddress ){
                return {...user , click: userClick.click}
            }else {
                return user
            }
        })
        socket.emit('scores',users);
      });

      setInterval(function(){   socket.emit('scores',users);},1000) //logs hi every second

    socket.on('disconnect',async  () => {
        users = users.filter(function( user ) {
            return user.ip !== userClick.id;
          });
        const newVal = await userClick.save()
        console.log("users : ",users)
      console.log('user disconnected ip  ', socket.request.connection.remoteAddress);
    });
  });


app.get('/', (req, res) => {
    res.send('Hello World!')
})

// app.get('/btn', async (req, res) => {
//     const clickModelInstance = await clickModel.findOne({ id: "0" })
//     if (clickModelInstance === null) {
//         console.log("seeding as failed")
//     }
//     res.send({ click: clickModelInstance.click })
// })
// app.post('/btn', async (req, res) => {
//     const clickModelInstance = await clickModel.findOne({ id: "0" })
//     let clickValFromDb = 0
//     if (clickModelInstance === null) {
//         console.log("seeding as failed")
//     } else {
//         clickModelInstance.click++
//         const newVal = await clickModelInstance.save()
//         clickValFromDb = newVal.click

//     }
//     res.send({ click: clickValFromDb })
// })

http.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})

async function  connectToDb() {
    try {
       await sequelize.authenticate();
        console.log('Connection has been established successfully.');
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
}
async function seed(){
    await clickModel.sync()
// const clickBdd = await clickModel.findByPk("0")
// if(clickBdd === null) await clickModel.create({ id: "0", click: 1 })
}